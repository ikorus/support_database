from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

from foe import views

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'supdb.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    
    url(r'^$', views.index, name='index'),
    url(r'^population/$', views.population, name='population'),
    url(r'^money/$', views.money, name='money'),
    url(r'^production/$', views.plant, name='plant'),
    url(r'^happiness/$', views.happiness, name='happiness'),
    url(r'^goods/$', views.goods, name='goods'),
    url(r'^goods/calc/$', views.calc, name='calc'),
    url(r'^greatb/$', views.greatb, name='greatb'),
    url(r'^greatb/priority/$', views.priority, name='priority'),
    url(r'^wiki/', include('foe.urls')),
    url(r'^admin/', include(admin.site.urls)),
    
    url(r'^accounts/', include('registration.backends.default.urls')),
    url(r'^accounts/profile/', views.profile, name='profile'),
)
