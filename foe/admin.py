# -*- coding: utf-8 -*-
# coding=utf-8
from django.contrib import admin
from django.db import models
from foe.models import Epoch, Technology, House, Society, Decor, Road, Plant
from foe.models import Source, GoodType, Manufactory, Production, GoodsCost
from foe.models import Legend, LegendLevel, LegendBonus, LegendBonusValue, LegendPrizeValue

class GoodsCostLegendInline(admin.TabularInline):
    model = GoodsCost
    extra = 0
    fields = ['good_type', 'quantity', ]
    
class GoodsCostTechInline(admin.TabularInline):
    model = GoodsCost
    extra = 0
    fields = ['good_type', 'quantity', ]

class BonusValueInline(admin.TabularInline):
    model = LegendBonusValue
    extra = 0

class PrizeValueInline(admin.TabularInline):
    model = LegendPrizeValue
    extra = 0

class ProductionPlantInline(admin.TabularInline):
    model = Production
    extra = 1
    max_num = 6
    fields = ['production_type', 'production_delay', 'quantity',]

class ProductionManufacturyInline(admin.TabularInline):
    model = Production
    extra = 1
    max_num = 4
    fields = ['production_type', 'production_delay', 'quantity', 'money_cost', 'resource_cost', 'good_cost', 'good_type']

class ProductionHouseInline(admin.TabularInline):
    model = Production
    extra = 1
    max_num = 1
    fields = ['production_type', 'production_delay', 'quantity',]

class TechnologyAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('name', 'epoch', 'parent', ('point_cost', 'money_cost', 'resource_cost', 'diamond_cost'), )
        }),
        (u'Дополнительно', {
            'classes': ('collapse',),
            'fields': ('desc', 'pict', )
        }),
    )
    inlines = [GoodsCostTechInline]
    list_display = ('name', 'epoch', 'point_cost',)
    list_filter = ['epoch']

class HouseAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': (
                'name', 
                ('epoch', 'technology'), 
                ('bonus', 'premium'), 
                ('width', 'height', 'road_width'), 
                ('money_cost', 'resource_cost', 'diamond_cost'),
                ('citizen_add', 'game_point', 'happiness_add'),
            )
        }),
        (u'Дополнительно', {
            'classes': ('collapse',),
            'fields': ('desc', 'pict', )
        }),
    )
    inlines = [ProductionHouseInline]
    list_filter = ['epoch']
    
class LegendLevelAdmin(admin.ModelAdmin):
    inlines = [BonusValueInline, PrizeValueInline]

class LegendAdmin(admin.ModelAdmin):
    inlines = [GoodsCostLegendInline]

class ManufacturyAdmin(admin.ModelAdmin):
    inlines = [ProductionManufacturyInline]
    list_display = ('name', 'epoch',)
    list_filter = ['epoch'] 
    
class PlantAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': (
                'name', 
                ('epoch', 'technology'), 
                ('bonus', 'premium'), 
                ('width', 'height', 'road_width'), 
                ('money_cost', 'resource_cost', 'citizen_cost', 'diamond_cost', 'happiness_add'),
                'game_point',
            )
        }),
        (u'Дополнительно', {
            'classes': ('collapse',),
            'fields': ('desc', 'pict', )
        }),
    )
    inlines = [ProductionPlantInline]
    list_filter = ['epoch']

admin.site.register(Epoch)
admin.site.register(Technology, TechnologyAdmin)
admin.site.register(House, HouseAdmin)
admin.site.register(Society)
admin.site.register(Decor)
admin.site.register(Road)
admin.site.register(Plant, PlantAdmin)
admin.site.register(Source)
admin.site.register(GoodType)
admin.site.register(Manufactory, ManufacturyAdmin)
admin.site.register(GoodsCost)
admin.site.register(Legend, LegendAdmin)
admin.site.register(LegendBonus)
admin.site.register(LegendLevel, LegendLevelAdmin)
