# -*- coding: utf-8 -*-
# coding=utf-8
from django import forms
from django.forms import ModelForm
from django.forms.formsets import formset_factory
from django.forms.models import inlineformset_factory
from foe.models import GoodType, Technology, GoodsCost


class MoneyForm(forms.Form):
    ENVIR = ((1, 'один раз в день'), (2, 'два раза в день'), (3, 'три раза в день'), )
    
    time = forms.IntegerField(widget=forms.NumberInput(attrs={'class': 'uk-form-width-mini uk-form-small uk-margin-left uk-margin-right', 'required': 'required'}), label=u"По сколько часов я играю", initial='16')
    envir = forms.TypedChoiceField(widget=forms.Select(attrs={'class': 'uk-form-small'}), coerce=int, choices=ENVIR, label=u"", empty_value=1)

    
class CalcForm(forms.Form):
    quantity = forms.IntegerField(widget=forms.NumberInput(attrs={'class': 'uk-form-small uk-margin-left uk-margin-right', 'style': 'width: 60px', 'required': 'required'}), label=u"Товар 1:", initial='1')
    good1 = forms.ModelChoiceField(queryset=GoodType.objects.order_by('id'), empty_label=None, label=u"", widget=forms.Select(attrs={'class': 'uk-form-small uk-margin-right'}))
    good2 = forms.ModelChoiceField(queryset=GoodType.objects.order_by('id'), empty_label=None, label=u"Товар 2:", widget=forms.Select(attrs={'class': 'uk-form-small uk-margin-left'}))

    
class TechForm(ModelForm):
    class Meta:
        model = Technology
        exclude = ['desc', 'pict', ]
        labels = {
            'name': (u'Название'),
            'epoch': (u'Эпоха'),
            'parent': (u'Предшествующие технологии'),
        }
        

class TechGoodsCostForm(ModelForm):
    class Meta:
        model = GoodsCost
        exclude = ['legend',]
        labels = {
            'good_type': (u'Товар'),
            'quantity': (u'Количество'),
        }