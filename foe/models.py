# -*- coding: utf-8 -*-
# coding=utf-8
from django.db import models


class Epoch(models.Model):
    name = models.CharField(max_length=200)
    desc = models.TextField(blank=True)
    def __unicode__(self):
        return self.name
    class Meta:
        verbose_name = 'Эпоха'
        verbose_name_plural = 'Эпохи'
        

class CommonInfo(models.Model):
    name = models.CharField(max_length=200)
    desc = models.TextField(blank=True)
    pict = models.CharField(max_length=255, blank=True)
    epoch = models.ForeignKey(Epoch)

    class Meta:
        abstract = True    
        
        
class Technology(CommonInfo):
    parent = models.ManyToManyField('self', symmetrical=False, blank=True)
    
    point_cost = models.PositiveSmallIntegerField(default=0)
    resource_cost = models.PositiveIntegerField(default=0)
    money_cost = models.PositiveIntegerField(default=0)
    diamond_cost = models.PositiveSmallIntegerField(default=0)
    
    def __unicode__(self):
        return self.name
    class Meta:
        verbose_name = 'Технология'
        verbose_name_plural = 'Технологии'
        

ROAD = ((1, 'одинарная'), (2, 'двойная'), )


class CommonInfoExt(CommonInfo):
    technology = models.ForeignKey(Technology, blank=True, null=True)
    bonus = models.BooleanField()
    premium = models.BooleanField()
    width = models.PositiveSmallIntegerField(default=0)
    height = models.PositiveSmallIntegerField(default=0)
    road_width = models.IntegerField(choices=ROAD)

    class Meta:
        abstract = True    


class House(CommonInfoExt):
    resource_cost = models.PositiveIntegerField(default=0)
    money_cost = models.PositiveIntegerField(default=0)
    diamond_cost = models.PositiveSmallIntegerField(default=0)

    citizen_add = models.PositiveSmallIntegerField(default=0)
    game_point = models.PositiveSmallIntegerField(default=0)
    happiness_add = models.SmallIntegerField(default=0)
    
    def __unicode__(self):
        return self.name
    class Meta:
        verbose_name = 'Дом'
        verbose_name_plural = 'Дома'
        

class Society(CommonInfoExt):
    resource_cost = models.PositiveSmallIntegerField(default=0)
    money_cost = models.PositiveIntegerField(default=0)
    diamond_cost = models.PositiveSmallIntegerField(default=0)

    happiness_add = models.PositiveSmallIntegerField(default=0)
    game_point = models.PositiveSmallIntegerField(default=0)
    
    def __unicode__(self):
        return self.name
    class Meta:
        verbose_name = 'Общественная постройка'
        verbose_name_plural = 'Общественные постройки'
        

class Decor(CommonInfo):
    technology = models.ForeignKey(Technology, blank=True, null=True)
    bonus = models.BooleanField()
    premium = models.BooleanField()
    width = models.PositiveSmallIntegerField(default=0)
    height = models.PositiveSmallIntegerField(default=0)
    
    resource_cost = models.PositiveSmallIntegerField(default=0)
    money_cost = models.PositiveSmallIntegerField(default=0)
    diamond_cost = models.PositiveSmallIntegerField(default=0)

    happiness_add = models.PositiveSmallIntegerField(default=0)
    game_point = models.PositiveSmallIntegerField(default=0)
    
    def __unicode__(self):
        return self.name
    class Meta:
        verbose_name = 'Декор'
        verbose_name_plural = 'Декор'
        

class Road(CommonInfo):
    technology = models.ForeignKey(Technology, blank=True, null=True)
    bonus = models.BooleanField()
    premium = models.BooleanField()
    width = models.PositiveSmallIntegerField(default=0)
    height = models.PositiveSmallIntegerField(default=0)
    
    resource_cost = models.PositiveSmallIntegerField(default=0)
    money_cost = models.PositiveSmallIntegerField(default=0)
    diamond_cost = models.PositiveSmallIntegerField(default=0)

    happiness_add = models.PositiveSmallIntegerField(default=0)
    game_point = models.PositiveSmallIntegerField(default=0)
    
    def __unicode__(self):
        return self.name
    class Meta:
        verbose_name = 'Дорога'
        verbose_name_plural = 'Дороги'
        

class Plant(CommonInfoExt):
    resource_cost = models.PositiveIntegerField(default=0)
    money_cost = models.PositiveIntegerField(default=0)
    diamond_cost = models.PositiveSmallIntegerField(default=0)
    citizen_cost = models.PositiveSmallIntegerField(default=0)

    game_point = models.PositiveSmallIntegerField(default=0)
    happiness_add = models.SmallIntegerField(blank=True, null=True)
    
    def __unicode__(self):
        return self.name
    class Meta:
        verbose_name = 'Производственная постройка'
        verbose_name_plural = 'Производственные постройки'
    

class Source(CommonInfo):
       
    def __unicode__(self):
        return self.name
    

class GoodType(CommonInfo):
    technology = models.ForeignKey(Technology)
    source = models.ForeignKey(Source)
       
    def __unicode__(self):
        return self.name
    

class Manufactory(CommonInfo):
    technology = models.ForeignKey(Technology)
    width = models.PositiveSmallIntegerField(default=0)
    height = models.PositiveSmallIntegerField(default=0)
    road_width = models.IntegerField(choices=ROAD)
    
    resource_cost = models.PositiveIntegerField(default=0)
    money_cost = models.PositiveIntegerField(default=0)
    diamond_cost = models.PositiveSmallIntegerField(default=0)
    citizen_cost = models.PositiveSmallIntegerField(default=0)

    game_point = models.PositiveSmallIntegerField(default=0)
    good_type = models.ForeignKey(GoodType)
    happiness_add = models.SmallIntegerField(blank=True, null=True)
    
    def __unicode__(self):
        return self.name
    class Meta:
        verbose_name = 'Промышленная постройка'
        verbose_name_plural = 'Промышленные постройки'
        

class Production(models.Model):
    TYPE = ((1, 'money'), (2, 'resource'), (3, 'good'), (4, 'point'), (5, 'medal'), )
    DELAY = ((300, '5 минут'), (900, '15 минут'), (3600, '1 час'), (14400, '4 часа'), (28800, '8 часов'), (86400, '24 часа'), (172800, '48 часов'),)
   
    house = models.ForeignKey(House, blank=True, null=True)
    plant = models.ForeignKey(Plant, blank=True, null=True)
    manufactory = models.ForeignKey(Manufactory, blank=True, null=True)

    production_type = models.IntegerField(choices=TYPE)
    production_delay = models.IntegerField(choices=DELAY)
    quantity = models.PositiveIntegerField(default=0)
    
    money_cost = models.PositiveSmallIntegerField(default=0)
    resource_cost = models.PositiveSmallIntegerField(default=0)
    good_cost = models.PositiveSmallIntegerField(default=0)
    good_type = models.ForeignKey(GoodType, blank=True, null=True)
    

class GoodsCost(models.Model):
    technology = models.ForeignKey(Technology, blank=True, null=True)
    legend = models.ForeignKey('Legend', blank=True, null=True)
    
    good_type = models.ForeignKey(GoodType)
    quantity = models.PositiveIntegerField(default=0)
    

class Legend(CommonInfo):
    width = models.PositiveSmallIntegerField(default=0)
    height = models.PositiveSmallIntegerField(default=0)
    road_width = models.IntegerField(choices=ROAD)
        
    def __unicode__(self):
        return self.name
    

LEVEL = ((1, '1'), (2, '2'), (3, '3'), (4, '4'), (5, '5'), (6, '6'), (7, '7'), (8, '8'), (9, '9'), (10, '10'), (11, '11'), (12, '12'), (13, '13'), (14, '14'), (15, '15'), (16, '16'), (17, '17'), (18, '18'), (19, '19'), (20, '20'), (21, '21'), (22, '22'), (23, '23'), (24, '24'), (25, '25'), )


class LegendLevel(models.Model):
    name = models.CharField(max_length=200, editable=False)
    LVL = 'level'
    legend = models.ForeignKey(Legend)
    
    level = models.IntegerField(choices=LEVEL)
    point_cost = models.PositiveSmallIntegerField(default=0)

    game_point = models.PositiveSmallIntegerField(default=0)

    def __unicode__(self):
        return self.name
    def save(self, **kwargs):
        self.name = "%s, %s %d" % (self.legend.name, self.LVL, self.level)
        super(LegendLevel, self).save(**kwargs)
    
    
class LegendBonus(models.Model):
    BONUS_TYPE_ACTIVE = (
                (1, 'Пассивный'), 
                (2, 'Производственный'), 
    )
    BONUS_TYPE_UNIQ = (
                (3, 'Уникальный'), 
                (4, 'Неуникальный'), 
    )
    BONUS_TYPE_CONVERT = (
                (5, 'Конвертируемый'), 
                (6, 'Неконвертируемый'), 
    )
    name = models.CharField(max_length=200)
    desc = models.TextField(blank=True)
    pict = models.CharField(max_length=255, blank=True)
        
    bonus_type_active = models.IntegerField(choices=BONUS_TYPE_ACTIVE, db_column='bonus_type')
    bonus_type_uniq = models.IntegerField(choices=BONUS_TYPE_UNIQ)
    bonus_type_convert = models.IntegerField(choices=BONUS_TYPE_CONVERT)
    
    default_value = models.PositiveSmallIntegerField(default=1)
    
    def __unicode__(self):
        return self.name
    

class LegendBonusValue(models.Model):
    legend_level = models.ForeignKey(LegendLevel)
    legend_bonus = models.ForeignKey(LegendBonus)
    
    quantity = models.PositiveIntegerField(default=0)

PRIZE_TYPE = (('None', 'Нет приза'), ('Blueprint', 'Чертежи'), ('Point', 'Стратегические очки'), ('Medal', 'Медали'), )
PRIZE_PLACE = ((1, 'Первое место'), (2, 'Второе место'), (3, 'Третье место'), (4, 'Четвертое место'), (5, 'Пятое место'), )

class LegendPrizeValue(models.Model):
    legend_level = models.ForeignKey(LegendLevel)
    prize_place = models.IntegerField(choices=PRIZE_PLACE)
    prize_type = models.CharField(max_length=30, choices=PRIZE_TYPE)
    
    quantity = models.PositiveIntegerField(default=0)