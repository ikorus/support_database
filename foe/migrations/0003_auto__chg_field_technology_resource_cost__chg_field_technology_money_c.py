# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Technology.resource_cost'
        db.alter_column(u'foe_technology', 'resource_cost', self.gf('django.db.models.fields.PositiveIntegerField')())

        # Changing field 'Technology.money_cost'
        db.alter_column(u'foe_technology', 'money_cost', self.gf('django.db.models.fields.PositiveIntegerField')())

    def backwards(self, orm):

        # Changing field 'Technology.resource_cost'
        db.alter_column(u'foe_technology', 'resource_cost', self.gf('django.db.models.fields.PositiveSmallIntegerField')())

        # Changing field 'Technology.money_cost'
        db.alter_column(u'foe_technology', 'money_cost', self.gf('django.db.models.fields.PositiveSmallIntegerField')())

    models = {
        u'foe.decor': {
            'Meta': {'object_name': 'Decor'},
            'bonus': ('django.db.models.fields.BooleanField', [], {}),
            'desc': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'diamond_cost': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'epoch': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.Epoch']"}),
            'game_point': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'happiness_add': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'height': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'money_cost': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'pict': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'premium': ('django.db.models.fields.BooleanField', [], {}),
            'resource_cost': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'technology': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.Technology']", 'null': 'True', 'blank': 'True'}),
            'width': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        u'foe.epoch': {
            'Meta': {'object_name': 'Epoch'},
            'desc': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'foe.goodscost': {
            'Meta': {'object_name': 'GoodsCost'},
            'good_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.GoodType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'legend': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.Legend']", 'null': 'True', 'blank': 'True'}),
            'quantity': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'technology': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.Technology']", 'null': 'True', 'blank': 'True'})
        },
        u'foe.goodtype': {
            'Meta': {'object_name': 'GoodType'},
            'desc': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'epoch': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.Epoch']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'pict': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'source': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.Source']"}),
            'technology': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.Technology']"})
        },
        u'foe.house': {
            'Meta': {'object_name': 'House'},
            'bonus': ('django.db.models.fields.BooleanField', [], {}),
            'citizen_add': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'desc': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'diamond_cost': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'epoch': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.Epoch']"}),
            'game_point': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'height': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'money_cost': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'pict': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'premium': ('django.db.models.fields.BooleanField', [], {}),
            'resource_cost': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'road_width': ('django.db.models.fields.IntegerField', [], {}),
            'technology': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.Technology']", 'null': 'True', 'blank': 'True'}),
            'width': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        u'foe.legend': {
            'Meta': {'object_name': 'Legend'},
            'desc': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'epoch': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.Epoch']"}),
            'height': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'pict': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'road_width': ('django.db.models.fields.IntegerField', [], {}),
            'width': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        u'foe.legendbonus': {
            'Meta': {'object_name': 'LegendBonus'},
            'bonus_type_active': ('django.db.models.fields.IntegerField', [], {'db_column': "'bonus_type'"}),
            'bonus_type_convert': ('django.db.models.fields.IntegerField', [], {}),
            'bonus_type_uniq': ('django.db.models.fields.IntegerField', [], {}),
            'default_value': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            'desc': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'pict': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'})
        },
        u'foe.legendbonusvalue': {
            'Meta': {'object_name': 'LegendBonusValue'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'legend_bonus': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.LegendBonus']"}),
            'legend_level': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.LegendLevel']"}),
            'quantity': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'})
        },
        u'foe.legendlevel': {
            'Meta': {'object_name': 'LegendLevel'},
            'game_point': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'legend': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.Legend']"}),
            'level': ('django.db.models.fields.IntegerField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'point_cost': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        u'foe.legendprizevalue': {
            'Meta': {'object_name': 'LegendPrizeValue'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'legend_level': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.LegendLevel']"}),
            'prize_place': ('django.db.models.fields.IntegerField', [], {}),
            'prize_type': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'quantity': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'})
        },
        u'foe.manufactory': {
            'Meta': {'object_name': 'Manufactory'},
            'citizen_cost': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'desc': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'diamond_cost': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'epoch': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.Epoch']"}),
            'game_point': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'good_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.GoodType']"}),
            'height': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'money_cost': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'pict': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'resource_cost': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'road_width': ('django.db.models.fields.IntegerField', [], {}),
            'technology': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.Technology']"}),
            'width': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        u'foe.plant': {
            'Meta': {'object_name': 'Plant'},
            'bonus': ('django.db.models.fields.BooleanField', [], {}),
            'citizen_cost': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'desc': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'diamond_cost': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'epoch': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.Epoch']"}),
            'game_point': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'height': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'money_cost': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'pict': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'premium': ('django.db.models.fields.BooleanField', [], {}),
            'resource_cost': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'road_width': ('django.db.models.fields.IntegerField', [], {}),
            'technology': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.Technology']", 'null': 'True', 'blank': 'True'}),
            'width': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        u'foe.production': {
            'Meta': {'object_name': 'Production'},
            'house': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.House']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'manufactory': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.Manufactory']", 'null': 'True', 'blank': 'True'}),
            'money_cost': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'plant': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.Plant']", 'null': 'True', 'blank': 'True'}),
            'production_delay': ('django.db.models.fields.IntegerField', [], {}),
            'production_type': ('django.db.models.fields.IntegerField', [], {}),
            'quantity': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'resource_cost': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        u'foe.road': {
            'Meta': {'object_name': 'Road'},
            'bonus': ('django.db.models.fields.BooleanField', [], {}),
            'desc': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'diamond_cost': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'epoch': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.Epoch']"}),
            'game_point': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'happiness_add': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'height': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'money_cost': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'pict': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'premium': ('django.db.models.fields.BooleanField', [], {}),
            'resource_cost': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'technology': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.Technology']", 'null': 'True', 'blank': 'True'}),
            'width': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        u'foe.society': {
            'Meta': {'object_name': 'Society'},
            'bonus': ('django.db.models.fields.BooleanField', [], {}),
            'desc': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'diamond_cost': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'epoch': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.Epoch']"}),
            'game_point': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'happiness_add': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'height': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'money_cost': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'pict': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'premium': ('django.db.models.fields.BooleanField', [], {}),
            'resource_cost': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'road_width': ('django.db.models.fields.IntegerField', [], {}),
            'technology': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.Technology']", 'null': 'True', 'blank': 'True'}),
            'width': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        u'foe.source': {
            'Meta': {'object_name': 'Source'},
            'desc': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'epoch': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.Epoch']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'pict': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'})
        },
        u'foe.technology': {
            'Meta': {'object_name': 'Technology'},
            'desc': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'diamond_cost': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'epoch': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.Epoch']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'money_cost': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'parent': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['foe.Technology']", 'symmetrical': 'False', 'blank': 'True'}),
            'pict': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'point_cost': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'resource_cost': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'})
        }
    }

    complete_apps = ['foe']