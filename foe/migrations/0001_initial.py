# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Epoch'
        db.create_table(u'foe_epoch', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('desc', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal(u'foe', ['Epoch'])

        # Adding model 'Technology'
        db.create_table(u'foe_technology', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('desc', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('pict', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('epoch', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['foe.Epoch'])),
            ('point_cost', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('resource_cost', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('money_cost', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('diamond_cost', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
        ))
        db.send_create_signal(u'foe', ['Technology'])

        # Adding M2M table for field parent on 'Technology'
        m2m_table_name = db.shorten_name(u'foe_technology_parent')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('from_technology', models.ForeignKey(orm[u'foe.technology'], null=False)),
            ('to_technology', models.ForeignKey(orm[u'foe.technology'], null=False))
        ))
        db.create_unique(m2m_table_name, ['from_technology_id', 'to_technology_id'])

        # Adding model 'House'
        db.create_table(u'foe_house', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('desc', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('pict', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('epoch', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['foe.Epoch'])),
            ('technology', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['foe.Technology'], null=True, blank=True)),
            ('bonus', self.gf('django.db.models.fields.BooleanField')()),
            ('premium', self.gf('django.db.models.fields.BooleanField')()),
            ('width', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('height', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('road_width', self.gf('django.db.models.fields.IntegerField')()),
            ('resource_cost', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
            ('money_cost', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
            ('diamond_cost', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('citizen_add', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('game_point', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
        ))
        db.send_create_signal(u'foe', ['House'])

        # Adding model 'Society'
        db.create_table(u'foe_society', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('desc', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('pict', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('epoch', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['foe.Epoch'])),
            ('technology', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['foe.Technology'], null=True, blank=True)),
            ('bonus', self.gf('django.db.models.fields.BooleanField')()),
            ('premium', self.gf('django.db.models.fields.BooleanField')()),
            ('width', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('height', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('road_width', self.gf('django.db.models.fields.IntegerField')()),
            ('resource_cost', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('money_cost', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
            ('diamond_cost', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('happiness_add', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('game_point', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
        ))
        db.send_create_signal(u'foe', ['Society'])

        # Adding model 'Decor'
        db.create_table(u'foe_decor', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('desc', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('pict', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('epoch', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['foe.Epoch'])),
            ('technology', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['foe.Technology'], null=True, blank=True)),
            ('bonus', self.gf('django.db.models.fields.BooleanField')()),
            ('premium', self.gf('django.db.models.fields.BooleanField')()),
            ('width', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('height', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('resource_cost', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('money_cost', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('diamond_cost', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('happiness_add', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('game_point', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
        ))
        db.send_create_signal(u'foe', ['Decor'])

        # Adding model 'Road'
        db.create_table(u'foe_road', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('desc', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('pict', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('epoch', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['foe.Epoch'])),
            ('technology', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['foe.Technology'], null=True, blank=True)),
            ('bonus', self.gf('django.db.models.fields.BooleanField')()),
            ('premium', self.gf('django.db.models.fields.BooleanField')()),
            ('width', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('height', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('resource_cost', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('money_cost', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('diamond_cost', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('happiness_add', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('game_point', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
        ))
        db.send_create_signal(u'foe', ['Road'])

        # Adding model 'Plant'
        db.create_table(u'foe_plant', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('desc', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('pict', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('epoch', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['foe.Epoch'])),
            ('technology', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['foe.Technology'], null=True, blank=True)),
            ('bonus', self.gf('django.db.models.fields.BooleanField')()),
            ('premium', self.gf('django.db.models.fields.BooleanField')()),
            ('width', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('height', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('road_width', self.gf('django.db.models.fields.IntegerField')()),
            ('resource_cost', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('money_cost', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('diamond_cost', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('citizen_cost', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('game_point', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
        ))
        db.send_create_signal(u'foe', ['Plant'])

        # Adding model 'Source'
        db.create_table(u'foe_source', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('desc', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('pict', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('epoch', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['foe.Epoch'])),
        ))
        db.send_create_signal(u'foe', ['Source'])

        # Adding model 'GoodType'
        db.create_table(u'foe_goodtype', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('desc', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('pict', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('epoch', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['foe.Epoch'])),
            ('technology', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['foe.Technology'])),
            ('source', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['foe.Source'])),
        ))
        db.send_create_signal(u'foe', ['GoodType'])

        # Adding model 'Manufactory'
        db.create_table(u'foe_manufactory', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('desc', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('pict', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('epoch', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['foe.Epoch'])),
            ('technology', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['foe.Technology'])),
            ('width', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('height', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('road_width', self.gf('django.db.models.fields.IntegerField')()),
            ('resource_cost', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('money_cost', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('diamond_cost', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('citizen_cost', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('game_point', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('good_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['foe.GoodType'])),
        ))
        db.send_create_signal(u'foe', ['Manufactory'])

        # Adding model 'Production'
        db.create_table(u'foe_production', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('house', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['foe.House'], null=True, blank=True)),
            ('plant', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['foe.Plant'], null=True, blank=True)),
            ('manufactory', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['foe.Manufactory'], null=True, blank=True)),
            ('production_type', self.gf('django.db.models.fields.IntegerField')()),
            ('production_delay', self.gf('django.db.models.fields.IntegerField')()),
            ('quantity', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
            ('money_cost', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('resource_cost', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
        ))
        db.send_create_signal(u'foe', ['Production'])

        # Adding model 'GoodsCost'
        db.create_table(u'foe_goodscost', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('technology', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['foe.Technology'], null=True, blank=True)),
            ('legend', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['foe.Legend'], null=True, blank=True)),
            ('good_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['foe.GoodType'])),
            ('quantity', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
        ))
        db.send_create_signal(u'foe', ['GoodsCost'])

        # Adding model 'Legend'
        db.create_table(u'foe_legend', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('desc', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('pict', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('epoch', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['foe.Epoch'])),
            ('width', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('height', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('road_width', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal(u'foe', ['Legend'])

        # Adding model 'LegendLevel'
        db.create_table(u'foe_legendlevel', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('legend', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['foe.Legend'])),
            ('level', self.gf('django.db.models.fields.IntegerField')()),
            ('point_cost', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('game_point', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
        ))
        db.send_create_signal(u'foe', ['LegendLevel'])

        # Adding model 'LegendBonus'
        db.create_table(u'foe_legendbonus', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('desc', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('pict', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('bonus_type_active', self.gf('django.db.models.fields.IntegerField')(db_column='bonus_type')),
            ('bonus_type_uniq', self.gf('django.db.models.fields.IntegerField')()),
            ('bonus_type_convert', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal(u'foe', ['LegendBonus'])

        # Adding model 'LegendBonusValue'
        db.create_table(u'foe_legendbonusvalue', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('legend_level', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['foe.LegendLevel'])),
            ('legend_bonus', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['foe.LegendBonus'])),
            ('quantity', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
        ))
        db.send_create_signal(u'foe', ['LegendBonusValue'])

        # Adding model 'LegendPrizeValue'
        db.create_table(u'foe_legendprizevalue', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('legend_level', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['foe.LegendLevel'])),
            ('prize_place', self.gf('django.db.models.fields.IntegerField')()),
            ('prize_type', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('quantity', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
        ))
        db.send_create_signal(u'foe', ['LegendPrizeValue'])


    def backwards(self, orm):
        # Deleting model 'Epoch'
        db.delete_table(u'foe_epoch')

        # Deleting model 'Technology'
        db.delete_table(u'foe_technology')

        # Removing M2M table for field parent on 'Technology'
        db.delete_table(db.shorten_name(u'foe_technology_parent'))

        # Deleting model 'House'
        db.delete_table(u'foe_house')

        # Deleting model 'Society'
        db.delete_table(u'foe_society')

        # Deleting model 'Decor'
        db.delete_table(u'foe_decor')

        # Deleting model 'Road'
        db.delete_table(u'foe_road')

        # Deleting model 'Plant'
        db.delete_table(u'foe_plant')

        # Deleting model 'Source'
        db.delete_table(u'foe_source')

        # Deleting model 'GoodType'
        db.delete_table(u'foe_goodtype')

        # Deleting model 'Manufactory'
        db.delete_table(u'foe_manufactory')

        # Deleting model 'Production'
        db.delete_table(u'foe_production')

        # Deleting model 'GoodsCost'
        db.delete_table(u'foe_goodscost')

        # Deleting model 'Legend'
        db.delete_table(u'foe_legend')

        # Deleting model 'LegendLevel'
        db.delete_table(u'foe_legendlevel')

        # Deleting model 'LegendBonus'
        db.delete_table(u'foe_legendbonus')

        # Deleting model 'LegendBonusValue'
        db.delete_table(u'foe_legendbonusvalue')

        # Deleting model 'LegendPrizeValue'
        db.delete_table(u'foe_legendprizevalue')


    models = {
        u'foe.decor': {
            'Meta': {'object_name': 'Decor'},
            'bonus': ('django.db.models.fields.BooleanField', [], {}),
            'desc': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'diamond_cost': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'epoch': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.Epoch']"}),
            'game_point': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'happiness_add': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'height': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'money_cost': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'pict': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'premium': ('django.db.models.fields.BooleanField', [], {}),
            'resource_cost': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'technology': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.Technology']", 'null': 'True', 'blank': 'True'}),
            'width': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        u'foe.epoch': {
            'Meta': {'object_name': 'Epoch'},
            'desc': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'foe.goodscost': {
            'Meta': {'object_name': 'GoodsCost'},
            'good_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.GoodType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'legend': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.Legend']", 'null': 'True', 'blank': 'True'}),
            'quantity': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'technology': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.Technology']", 'null': 'True', 'blank': 'True'})
        },
        u'foe.goodtype': {
            'Meta': {'object_name': 'GoodType'},
            'desc': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'epoch': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.Epoch']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'pict': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'source': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.Source']"}),
            'technology': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.Technology']"})
        },
        u'foe.house': {
            'Meta': {'object_name': 'House'},
            'bonus': ('django.db.models.fields.BooleanField', [], {}),
            'citizen_add': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'desc': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'diamond_cost': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'epoch': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.Epoch']"}),
            'game_point': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'height': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'money_cost': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'pict': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'premium': ('django.db.models.fields.BooleanField', [], {}),
            'resource_cost': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'road_width': ('django.db.models.fields.IntegerField', [], {}),
            'technology': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.Technology']", 'null': 'True', 'blank': 'True'}),
            'width': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        u'foe.legend': {
            'Meta': {'object_name': 'Legend'},
            'desc': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'epoch': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.Epoch']"}),
            'height': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'pict': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'road_width': ('django.db.models.fields.IntegerField', [], {}),
            'width': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        u'foe.legendbonus': {
            'Meta': {'object_name': 'LegendBonus'},
            'bonus_type_active': ('django.db.models.fields.IntegerField', [], {'db_column': "'bonus_type'"}),
            'bonus_type_convert': ('django.db.models.fields.IntegerField', [], {}),
            'bonus_type_uniq': ('django.db.models.fields.IntegerField', [], {}),
            'desc': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'pict': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'})
        },
        u'foe.legendbonusvalue': {
            'Meta': {'object_name': 'LegendBonusValue'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'legend_bonus': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.LegendBonus']"}),
            'legend_level': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.LegendLevel']"}),
            'quantity': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'})
        },
        u'foe.legendlevel': {
            'Meta': {'object_name': 'LegendLevel'},
            'game_point': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'legend': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.Legend']"}),
            'level': ('django.db.models.fields.IntegerField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'point_cost': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        u'foe.legendprizevalue': {
            'Meta': {'object_name': 'LegendPrizeValue'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'legend_level': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.LegendLevel']"}),
            'prize_place': ('django.db.models.fields.IntegerField', [], {}),
            'prize_type': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'quantity': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'})
        },
        u'foe.manufactory': {
            'Meta': {'object_name': 'Manufactory'},
            'citizen_cost': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'desc': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'diamond_cost': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'epoch': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.Epoch']"}),
            'game_point': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'good_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.GoodType']"}),
            'height': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'money_cost': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'pict': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'resource_cost': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'road_width': ('django.db.models.fields.IntegerField', [], {}),
            'technology': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.Technology']"}),
            'width': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        u'foe.plant': {
            'Meta': {'object_name': 'Plant'},
            'bonus': ('django.db.models.fields.BooleanField', [], {}),
            'citizen_cost': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'desc': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'diamond_cost': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'epoch': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.Epoch']"}),
            'game_point': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'height': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'money_cost': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'pict': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'premium': ('django.db.models.fields.BooleanField', [], {}),
            'resource_cost': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'road_width': ('django.db.models.fields.IntegerField', [], {}),
            'technology': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.Technology']", 'null': 'True', 'blank': 'True'}),
            'width': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        u'foe.production': {
            'Meta': {'object_name': 'Production'},
            'house': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.House']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'manufactory': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.Manufactory']", 'null': 'True', 'blank': 'True'}),
            'money_cost': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'plant': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.Plant']", 'null': 'True', 'blank': 'True'}),
            'production_delay': ('django.db.models.fields.IntegerField', [], {}),
            'production_type': ('django.db.models.fields.IntegerField', [], {}),
            'quantity': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'resource_cost': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        u'foe.road': {
            'Meta': {'object_name': 'Road'},
            'bonus': ('django.db.models.fields.BooleanField', [], {}),
            'desc': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'diamond_cost': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'epoch': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.Epoch']"}),
            'game_point': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'happiness_add': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'height': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'money_cost': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'pict': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'premium': ('django.db.models.fields.BooleanField', [], {}),
            'resource_cost': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'technology': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.Technology']", 'null': 'True', 'blank': 'True'}),
            'width': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        u'foe.society': {
            'Meta': {'object_name': 'Society'},
            'bonus': ('django.db.models.fields.BooleanField', [], {}),
            'desc': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'diamond_cost': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'epoch': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.Epoch']"}),
            'game_point': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'happiness_add': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'height': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'money_cost': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'pict': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'premium': ('django.db.models.fields.BooleanField', [], {}),
            'resource_cost': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'road_width': ('django.db.models.fields.IntegerField', [], {}),
            'technology': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.Technology']", 'null': 'True', 'blank': 'True'}),
            'width': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        u'foe.source': {
            'Meta': {'object_name': 'Source'},
            'desc': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'epoch': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.Epoch']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'pict': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'})
        },
        u'foe.technology': {
            'Meta': {'object_name': 'Technology'},
            'desc': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'diamond_cost': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'epoch': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foe.Epoch']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'money_cost': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'parent': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['foe.Technology']", 'symmetrical': 'False', 'blank': 'True'}),
            'pict': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'point_cost': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'resource_cost': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        }
    }

    complete_apps = ['foe']