# -*- coding: utf-8 -*-
# coding=utf-8
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from django.forms.models import inlineformset_factory
from django.db.models import Sum, Count

from foe.models import Epoch, Technology, GoodsCost, House, Production, Society, Decor, Road, Plant, Manufactory, Source, Legend, LegendLevel, LegendBonus, LegendBonusValue, LegendPrizeValue, GoodType
from foe.forms import MoneyForm, CalcForm, TechForm, TechGoodsCostForm


def profile(request):
    if request.method == 'POST':
        form = TechForm(request.POST or None)
        if form.is_valid():
            form.save()
    else:
        form = TechForm()
    
    if request.method == 'POST':
        form2 = TechGoodsCostForm(request.POST or None)
        if form.is_valid():
            form.save()
    else:
        form2 = TechGoodsCostForm()
    
    GoodsCostFormSet = inlineformset_factory(Technology, GoodsCost)
    if request.method == 'POST':
        formset = GoodsCostFormSet(request.POST or None)
        if form.is_valid():
            formset.save()
    else:
        formset = GoodsCostFormSet
    
    context = {
        'form': form,
        'form2': form2,
        'formset': formset,
    }
    return render(request, "user/profile.html", context)
    
    
def wiki(request):
    return render(request, "foe/wiki.html")

    
def index(request):
    legend_list_money = Legend.objects.filter(legendlevel__legendbonusvalue__legend_bonus__id=8)[:1]
    legend_list_medal = Legend.objects.filter(legendlevel__legendbonusvalue__legend_bonus__id=5)[:1]
    legend_list_money_inflow = Legend.objects.filter(legendlevel__legendbonusvalue__legend_bonus__id=9)[:1]
    legend_list_point = Legend.objects.filter(legendlevel__legendbonusvalue__legend_bonus__id=7)[:1]
    
    context = {
        'legend_list_money': legend_list_money,
        'legend_list_medal': legend_list_medal,
        'legend_list_money_inflow': legend_list_money_inflow,
        'legend_list_point': legend_list_point,
    }
    return render(request, "foe/index.html", context)
    
    
def population(request):
    legend_list_population = Legend.objects.filter(legendlevel__legendbonusvalue__legend_bonus__id=1)[:1]
    house_list = House.objects.order_by('id').values('name', 'bonus', 'premium', 'width', 'height', 'road_width', 'citizen_add', 'epoch__name')
    for i in house_list:
        i['s'] = i['width'] * i['height']
        if i['width'] > i['height']:
            i['r'] = i['width'] * i['road_width']
        else:
            i['r'] = i['height'] * i['road_width']
        i['e'] = float(i['citizen_add']) / float((i['s'] + i['r']))
        i['epoch'] = i['epoch__name']
    
    house_list = sorted(house_list, key=lambda k: k['e'], reverse=True)
    
    legend_list = LegendLevel.objects.filter(legendbonusvalue__legend_bonus__id=1).values('legend__name', 'level', 'legend__epoch__name', 'legend__width', 'legend__height', 'legend__road_width', 'legendbonusvalue__quantity')
    for i in legend_list:
        i['s'] = i['legend__width'] * i['legend__height']
        if i['legend__width'] > i['legend__height']:
            i['r'] = i['legend__width'] * i['legend__road_width']
        else:
            i['r'] = i['legend__height'] * i['legend__road_width']
        i['e'] = float(i['legendbonusvalue__quantity']) / float((i['s'] + i['r']))

    legend_list = sorted(legend_list, key=lambda k: k['e'], reverse=True)
    
    house_list.extend(legend_list)
    house_list = sorted(house_list, key=lambda k: k['e'], reverse=True)
    
    context = {
        'legend_list': legend_list,
        'house_list': house_list,
        'legend_list_population': legend_list_population,
    }
    return render(request, "foe/population.html", context)

    
def money(request):
    time = 16
    envir = 1
    
    if request.method == 'GET':
        form = MoneyForm(request.GET or None)
        if form.is_valid():
            time = form.cleaned_data['time']
            envir = form.cleaned_data['envir']
    else:
        form = MoneyForm()
    
    if time > 24 and envir == 1:
        alert = 1
    elif time > 12 and envir == 2:
        alert = 2
    elif time > 8 and envir == 3:
        alert = 3
    else:
        alert = 0
    time = time * 3600
    
    legend_list_money = Legend.objects.filter(legendlevel__legendbonusvalue__legend_bonus__id=8)[:1]
    legend_list_money_inflow = Legend.objects.filter(legendlevel__legendbonusvalue__legend_bonus__id=9)[:1]
    
    house_list = House.objects.order_by('id').filter(production__production_type=1).values('name', 'bonus', 'premium', 'width', 'height', 'road_width', 'epoch__name', 'production__quantity', 'production__production_delay')
    for i in house_list:
        i['s'] = i['width'] * i['height']
        if i['width'] > i['height']:
            i['r'] = i['width'] * i['road_width']
        else:
            i['r'] = i['height'] * i['road_width']
            
        if envir == 1 and time < 86400 or envir == 2 and time < 43200 or envir == 3 and time < 28800:
            i['e'] = float(i['production__quantity']) * ((1 + (time / i['production__production_delay'])) * envir) / float((i['s'] + i['r']))
        else:
            i['e'] = float(i['production__quantity'] * 86400 / i['production__production_delay']) / float((i['s'] + i['r']))
        
        i['epoch'] = i['epoch__name']
    
    house_list = sorted(house_list, key=lambda k: k['e'], reverse=True)
    
    legend_list = LegendLevel.objects.filter(legendbonusvalue__legend_bonus__id=8).values('legend__name', 'level', 'legend__epoch__name', 'legend__width', 'legend__height', 'legend__road_width', 'legendbonusvalue__quantity')
    for i in legend_list:
        i['s'] = i['legend__width'] * i['legend__height']
        if i['legend__width'] > i['legend__height']:
            i['r'] = i['legend__width'] * i['legend__road_width']
        else:
            i['r'] = i['legend__height'] * i['legend__road_width']
        i['e'] = float(i['legendbonusvalue__quantity']) / float((i['s'] + i['r']))

    legend_list = sorted(legend_list, key=lambda k: k['e'], reverse=True)
    
    house_list.extend(legend_list)
    house_list = sorted(house_list, key=lambda k: k['e'], reverse=True)

    context = {
        'legend_list': legend_list,
        'house_list': house_list,
        'legend_list_money': legend_list_money,
        'legend_list_money_inflow': legend_list_money_inflow,
        'form': form,
        'alert': alert,
    }
    return render(request, "foe/money.html", context)    

    
def plant(request):
    
    legend_list_resource = Legend.objects.filter(legendlevel__legendbonusvalue__legend_bonus__id=10)[:1]
    legend_list_resource_inflow = Legend.objects.filter(legendlevel__legendbonusvalue__legend_bonus__id=6)[:1]
    
    production_list = Production.objects.filter(production_delay=86400).filter(production_type=2)
    production_list_t = production_list.values('plant__name', 'plant__bonus', 'plant__premium', 'plant__width', 'plant__height', 'plant__road_width', 'plant__epoch__name', 'quantity')
    for i in production_list_t:
        i['s'] = i['plant__width'] * i['plant__height']
        if i['plant__width'] > i['plant__height']:
            i['r'] = i['plant__width'] * i['plant__road_width']
        else:
            i['r'] = i['plant__height'] * i['plant__road_width']
        i['s'] = i['s'] + i['r']
        i['e'] = float(i['quantity'] / i['s'])
        
    plant_list = sorted(production_list_t, key=lambda k: k['e'], reverse=True)
    
    legend_list = LegendLevel.objects.filter(legendbonusvalue__legend_bonus__id=10).values('legend__name', 'level', 'legend__epoch__name', 'legend__width', 'legend__height', 'legend__road_width', 'legendbonusvalue__quantity')
    for i in legend_list:
        i['s'] = i['legend__width'] * i['legend__height']
        if i['legend__width'] > i['legend__height']:
            i['r'] = i['legend__width'] * i['legend__road_width']
        else:
            i['r'] = i['legend__height'] * i['legend__road_width']
        i['s'] = i['s'] + i['r']
        i['e'] = float(i['legendbonusvalue__quantity'] / i['s'])

    plant_list.extend(legend_list)
    plant_list = sorted(plant_list, key=lambda k: k['e'], reverse=True)

    production_list_p = production_list.values('plant__name', 'plant__bonus', 'plant__premium', 'plant__citizen_cost', 'plant__epoch__name', 'quantity')
    for i in production_list_p:
        i['e'] = float(i['quantity'] / i['plant__citizen_cost'])
        
    plant_list_p = sorted(production_list_p, key=lambda k: k['e'], reverse=True)
    
    context = {
        'legend_list': legend_list,
        'plant_list': plant_list,
        'plant_list_p': plant_list_p,
        'legend_list_resource': legend_list_resource,
        'legend_list_resource_inflow': legend_list_resource_inflow,
    }
    return render(request, "foe/plant.html", context) 

    
def happiness(request):
    
    legend_list_happy = LegendBonus.objects.filter(id=4).filter(legendbonusvalue__legend_level__level=1).values('legendbonusvalue__legend_level__legend__name', 'legendbonusvalue__legend_level__legend__id')
    
    happy_list = Society.objects.order_by('id').values('name', 'bonus', 'premium', 'width', 'height', 'road_width', 'epoch__name', 'happiness_add')
    for i in happy_list:
        i['s'] = i['width'] * i['height']
        if i['width'] > i['height']:
            i['r'] = i['width'] * i['road_width']
        else:
            i['r'] = i['height'] * i['road_width']
        i['s'] = float(i['s'] + i['r'])
        i['e'] = float(i['happiness_add'] / i['s'])
        i['type'] = u'Общественная постройка'
        
    happy_list = sorted(happy_list, key=lambda k: k['e'], reverse=True)
    
    dekor_list = Decor.objects.order_by('id').values('name', 'bonus', 'premium', 'width', 'height', 'epoch__name', 'happiness_add')
    for i in dekor_list:
        i['s'] = float(i['width'] * i['height'])
        i['e'] = float(i['happiness_add'] / i['s'])
        i['type'] = u'Декор'
        
    happy_list.extend(dekor_list)
    happy_list = sorted(happy_list, key=lambda k: k['e'], reverse=True)
    
    road_list = Road.objects.order_by('id').values('name', 'bonus', 'premium', 'width', 'height', 'epoch__name', 'happiness_add')
    for i in road_list:
        i['s'] = float(i['width'] * i['height'])
        i['e'] = float(i['happiness_add'] / i['s'])
        i['type'] = u'Дорога'
        
    happy_list.extend(road_list)
    happy_list = sorted(happy_list, key=lambda k: k['e'], reverse=True)
    
    legend_list = LegendLevel.objects.filter(legendbonusvalue__legend_bonus__id=4).values('legend__name', 'level', 'legend__epoch__name', 'legend__width', 'legend__height', 'legend__road_width', 'legendbonusvalue__quantity')
    for i in legend_list:
        i['s'] = i['legend__width'] * i['legend__height']
        if i['legend__width'] > i['legend__height']:
            i['r'] = i['legend__width'] * i['legend__road_width']
        else:
            i['r'] = i['legend__height'] * i['legend__road_width']
        i['s'] = float(i['s'] + i['r'])
        i['e'] = float(i['legendbonusvalue__quantity'] / i['s'])
        i['type'] = u'Великое Строение'
        
    happy_list.extend(legend_list)
    happy_list = sorted(happy_list, key=lambda k: k['e'], reverse=True)
    
    baff_list = Society.objects.order_by('-happiness_add').values('name', 'bonus', 'premium', 'epoch__name', 'happiness_add')
    for i in baff_list:
        i['type'] = u'Общественная постройка'
    baff_list = sorted(baff_list, key=lambda k: k['happiness_add'], reverse=True)
    dekor_baff_list = Decor.objects.order_by('id').values('name', 'bonus', 'premium', 'epoch__name', 'happiness_add')
    for i in dekor_baff_list:
        i['type'] = u'Декор'
    baff_list.extend(dekor_baff_list)
    baff_list = sorted(baff_list, key=lambda k: k['happiness_add'], reverse=True)
    
    context = {
        'legend_list_h': legend_list_happy,
        'happy_list': happy_list,
        'baff_list': baff_list,
    }
    return render(request, "foe/happiness.html", context) 

 
def goods(request):
    
    legend_list_goods = LegendBonus.objects.filter(id=2).filter(legendbonusvalue__legend_level__level=1).values('legendbonusvalue__legend_level__legend__name', 'legendbonusvalue__legend_level__legend__id')
    epoch_list = Epoch.objects.order_by('id').values('id', 'name', )
    for i in epoch_list:
        i['goods'] = u', '.join(n.name for n in GoodType.objects.filter(epoch__id=i['id']))
        i['var'] = Manufactory.objects.filter(epoch__id=i['id'])
        for k in i['var']:
            k.cost = Production.objects.filter(manufactory=k).filter(quantity=10)
            for j in k.cost:
                j.money_cost = j.money_cost / j.quantity
                j.resource_cost = j.resource_cost / j.quantity
        
    context = {
        'legend_list_goods': legend_list_goods,
        'epoch_list': epoch_list,
    }
    return render(request, "foe/goods.html", context) 
    

def calc(request):
    quantity = 1
    data = 0
    if request.method == 'GET':
        form = CalcForm(request.GET or None)
        if form.is_valid():
            quantity = form.cleaned_data['quantity']
            good1 = form.cleaned_data['good1']
            good2 = form.cleaned_data['good2']
            data = 1
    else:
        form = CalcForm()
    
    try:
        g1 = get_object_or_404(Manufactory, good_type__name=good1)
        g1.cost = get_object_or_404(Production, manufactory=g1, quantity=10)
        
        if g1.cost.good_type:
            m1 = Manufactory.objects.filter(good_type=g1.cost.good_type)
            p1 = Production.objects.filter(manufactory=m1).filter(quantity=10)
            pr1 = get_object_or_404(Production, manufactory=m1, quantity=10)
            if pr1.good_type:
                mm1 = Manufactory.objects.filter(good_type=pr1.good_type)
                pp1 = Production.objects.filter(manufactory=mm1).filter(quantity=10)
                for i in pp1:
                    g1.cost.cost = quantity * (float(g1.cost.money_cost / g1.cost.quantity) + float(pr1.money_cost / pr1.quantity) + float(i.money_cost / i.quantity))
            else:
                for i in p1:
                    g1.cost.cost = quantity * (float(g1.cost.money_cost / g1.cost.quantity) + float(i.money_cost / i.quantity))
        else:
            g1.cost.cost = quantity * float(g1.cost.money_cost / g1.cost.quantity)
        
        g2 = get_object_or_404(Manufactory, good_type__name=good2)
        g2.cost = get_object_or_404(Production, manufactory=g2, quantity=10)
        
        if g2.cost.good_type:
            m2 = Manufactory.objects.filter(good_type=g2.cost.good_type)
            p2 = Production.objects.filter(manufactory=m2).filter(quantity=10)
            pr2 = get_object_or_404(Production, manufactory=m2, quantity=10)
            if pr2.good_type:
                mm2 = Manufactory.objects.filter(good_type=pr2.good_type)
                pp2 = Production.objects.filter(manufactory=mm2).filter(quantity=10)
                for i in pp2:
                    g2.cost.cost = (float(g2.cost.money_cost / g2.cost.quantity) + float(pr2.money_cost / pr2.quantity) + float(i.money_cost / i.quantity))
            else:
                for i in p2:
                    g2.cost.cost = (float(g2.cost.money_cost / g2.cost.quantity) + float(i.money_cost / i.quantity))
        else:
            g2.cost.cost = float(g2.cost.money_cost / g2.cost.quantity)

        g1_quantity = quantity
        g1_name = good1
        g2_quantity = float(g1.cost.cost / g2.cost.cost)
        g2_name = good2
    except:
        g1_quantity = 0
        g1_name = 0
        g2_quantity = 0
        g2_name = 0
    
    epoch_list = Epoch.objects.order_by('id').values('id', 'name', )
    for i in epoch_list:
        i['goods'] = u', '.join(n.name for n in GoodType.objects.filter(epoch__id=i['id']))
        i['var'] = Manufactory.objects.filter(epoch__id=i['id'])
        for k in i['var']:
            k.cost = Production.objects.filter(manufactory=k).filter(quantity=10)
            for j in k.cost:
                j.money_cost = j.money_cost / j.quantity
                j.resource_cost = j.resource_cost / j.quantity
                j.good_cost = j.good_cost / j.quantity
                j.good_type = j.good_type
                j.m = Manufactory.objects.filter(good_type=j.good_type)
                j.p = Production.objects.filter(manufactory=j.m).filter(quantity=10)
                for p in j.p:
                    p.money_cost = p.money_cost / p.quantity + j.money_cost
                    p.resource_cost = p.resource_cost / p.quantity + j.resource_cost
                    p.good_cost = p.good_cost / p.quantity
                    p.good_type = p.good_type
                    p.m = Manufactory.objects.filter(good_type=p.good_type)
                    p.p = Production.objects.filter(manufactory=p.m).filter(quantity=10)
                    for n in p.p:
                        n.money_cost = n.money_cost / n.quantity + p.money_cost
                        n.resource_cost = n.resource_cost / n.quantity + p.resource_cost
        
    context = {
        'epoch_list': epoch_list,
        'form': form,
        'g1_quantity': g1_quantity,
        'g1_name': g1_name,
        'g2_quantity': g2_quantity,
        'g2_name': g2_name,
        'data': data,
    }
    return render(request, "foe/calc.html", context) 


def greatb(request):
    bonus_list = LegendBonus.objects.order_by('id')
    bonus_list_pass = bonus_list.filter(bonus_type_active=1)
    bonus_list_prod = bonus_list.filter(bonus_type_active=2)
    bonus_list_uniq = bonus_list.filter(bonus_type_uniq=3)
    bonus_list_ununiq = bonus_list.filter(bonus_type_uniq=4)
    bonus_list_convert = bonus_list.filter(bonus_type_convert=5)
    bonus_list_unconvert = bonus_list.filter(bonus_type_convert=6)
    epoch_list = Epoch.objects.order_by('id')
    for i in epoch_list:
        i.legend = Legend.objects.filter(epoch=i)
        for k in i.legend:
            k.goodcost = GoodsCost.objects.filter(legend=k)
            k.legend_level = LegendLevel.objects.filter(legend=k)[:1]
            for m in k.legend_level:
                m.legend_bonus = LegendBonusValue.objects.filter(legend_level=m)
    for i in bonus_list:
        i.legend_list = LegendLevel.objects.filter(level=1).filter(legendbonusvalue__legend_bonus=i).values('legend__name', 'level', 'legend__epoch__name', 'legend__width', 'legend__height', 'legend__road_width', 'legendbonusvalue__quantity')
    
    legend_list = Legend.objects.order_by('id')
    context = {
        'legend_list': legend_list,
        'epoch_list': epoch_list,
        'bonus_list': bonus_list,
        'bonus_pass': bonus_list_pass,
        'bonus_prod': bonus_list_prod,
        'bonus_uniq': bonus_list_uniq,
        'bonus_ununiq': bonus_list_ununiq,
        'bonus_convert': bonus_list_convert,
        'bonus_unconvert': bonus_list_unconvert,
    }
    return render(request, "foe/greatb.html", context)
 

def priority(request):
    bonus_list = LegendBonus.objects.order_by('id')
    legend_list = Legend.objects.order_by('id')
    for i in legend_list:
        i.bonus_list = LegendLevel.objects.filter(legend=i).filter(level=1).values('name', 'legendbonusvalue__legend_bonus__name', 'legendbonusvalue__legend_bonus__default_value', )
        for j in i.bonus_list:
            j['length'] = len(i.bonus_list)
    
    
    epoch_list = Epoch.objects.order_by('id')
    for i in epoch_list:
        i.legend = Legend.objects.filter(epoch=i)
        for k in i.legend:
            k.goodcost = GoodsCost.objects.filter(legend=k)
            k.legend_level = LegendLevel.objects.filter(legend=k)[:1]
            for m in k.legend_level:
                m.legend_bonus = LegendBonusValue.objects.filter(legend_level=m)
    for i in bonus_list:
        i.legend_list = LegendLevel.objects.filter(level=1).filter(legendbonusvalue__legend_bonus=i).values('legend__name', 'level', 'legend__epoch__name', 'legend__width', 'legend__height', 'legend__road_width', 'legendbonusvalue__quantity')
    
    
    context = {
        'legend_list': legend_list,
        'epoch_list': epoch_list,
        'bonus_list': bonus_list,
    }
    return render(request, "foe/priority.html", context)
 
    
def epoch(request):
    epoch_list = Epoch.objects.order_by('id')
    context = {'epoch_list': epoch_list}
    return render(request, "foe/epoch.html", context)
    
def epoch_detail(request, epoch_id):
    epoch = get_object_or_404(Epoch, pk=epoch_id)
    tech = Technology.objects.filter(epoch=epoch_id)
    ret = {}
    names = GoodsCost.objects.filter(technology__epoch=epoch_id).order_by('good_type').values_list('good_type__name', flat=True)
    for name in set(list(names)):
        ret[name] = GoodsCost.objects.filter(technology__epoch=epoch_id, good_type__name=name).aggregate(q=Sum('quantity'))['q']
    point_cost = tech.aggregate(Sum('point_cost')).values()
    tech_count = tech.aggregate(Count('id')).values()
    resource_cost = tech.aggregate(Sum('resource_cost')).values()
    money_cost = tech.aggregate(Sum('money_cost')).values()
    diamond_cost = tech.aggregate(Sum('diamond_cost')).values()
    house_list = House.objects.filter(epoch=epoch_id).values('name', 'bonus', 'premium', 'width', 'height', 'road_width', 'citizen_add', 'production__quantity', 'production__production_delay')
    for i in house_list:
        i['s'] = i['width'] * i['height']
        if i['width'] > i['height']:
            i['r'] = i['width'] * i['road_width']
        else:
            i['r'] = i['height'] * i['road_width']
        i['e'] = float(i['citizen_add']) / float((i['s'] + i['r']))
        i['production_delay_m'] = i['production__production_delay'] / 60
        i['time'] = u'м'
        if i['production_delay_m'] >= 60:
            i['production_delay_h'] = i['production_delay_m'] / 60
            i['time'] = u'ч'
    plant_list = Plant.objects.filter(epoch=epoch_id)
    production_list = Production.objects.filter(production_delay=86400).filter(production_type=2).filter(plant__epoch=epoch_id)
    production_list_t = production_list.values('plant__name', 'plant__bonus', 'plant__premium', 'plant__width', 'plant__height', 'plant__road_width', 'plant__citizen_cost', 'quantity')
    for i in production_list_t:
        i['s'] = i['plant__width'] * i['plant__height']
        if i['plant__width'] > i['plant__height']:
            i['r'] = i['plant__width'] * i['plant__road_width']
        else:
            i['r'] = i['plant__height'] * i['plant__road_width']
        i['s'] = i['s'] + i['r']
        i['e'] = float(i['quantity'] / i['s'])
        i['c'] = float(i['quantity'] / i['plant__citizen_cost'])
    society_list = Society.objects.filter(epoch=epoch_id).values('name', 'bonus', 'premium', 'width', 'height', 'road_width', 'epoch__name', 'happiness_add')
    for i in society_list:
        i['s'] = i['width'] * i['height']
        if i['width'] > i['height']:
            i['r'] = i['width'] * i['road_width']
        else:
            i['r'] = i['height'] * i['road_width']
        i['s'] = i['s'] + i['r']
        i['e'] = float(i['happiness_add'] / i['s'])
    dekor_list = Decor.objects.filter(epoch=epoch_id).values('name', 'bonus', 'premium', 'width', 'height', 'epoch__name', 'happiness_add')
    for i in dekor_list:
        i['s'] = i['width'] * i['height']
        i['e'] = float(i['happiness_add'] / i['s'])
        i['type'] = u'Декор'
    legend_list = Legend.objects.filter(epoch=epoch_id)
    for i in legend_list:
        i.legend_level = LegendLevel.objects.filter(legend=i)[:1]
        for k in i.legend_level:
            k.legend_bonus = LegendBonusValue.objects.filter(legend_level=k)
    
    context = {
        'epoch': epoch, 
        'tech': tech, 
        'point_cost': point_cost,
        'resource_cost': resource_cost,
        'money_cost': money_cost,
        'diamond_cost': diamond_cost,
        'tech_count': tech_count,
        'names': ret.iteritems(),
        'house_list': house_list,
        'plant_list': plant_list,
        'production_list': production_list_t,
        'society_list': society_list,
        'dekor_list': dekor_list,
        'legend_list': legend_list,
    }
    return render(request, "foe/epoch_detail.html", context)
    
def technology(request):
    epoch_list = Epoch.objects.order_by('id')
    for i in epoch_list:
        i.tech = Technology.objects.filter(epoch=i)
        for k in i.tech:
            k.goodcost = GoodsCost.objects.filter(technology=k)

    tech_list = Technology.objects.order_by('id')
    context = {
        'tech_list': tech_list,
        'epoch_list': epoch_list
    }
    return render(request, "foe/technology.html", context)
    
def houses(request):
    epoch_list = Epoch.objects.order_by('id')
    for i in epoch_list:
        i.house = House.objects.filter(epoch=i)
        for k in i.house:
            k.production = Production.objects.filter(house=k)
            for n in k.production:
                n.production_delay_m = n.production_delay / 60
                n.time = 'м'
                if n.production_delay_m >= 60:
                    n.production_delay_h = n.production_delay_m / 60
                    n.time = 'ч'
                 

    houses_list = House.objects.order_by('id')
    context = {
        'houses_list': houses_list,
        'epoch_list': epoch_list
    }
    return render(request, "foe/houses.html", context)
    
def society(request):
    epoch_list = Epoch.objects.order_by('id')
    for i in epoch_list:
        i.society = Society.objects.filter(epoch=i)

    society_list = Society.objects.order_by('id')
    context = {
        'society_list': society_list,
        'epoch_list': epoch_list
    }
    return render(request, "foe/society.html", context)
    
def decor(request):
    epoch_list = Epoch.objects.order_by('id')
    for i in epoch_list:
        i.decor = Decor.objects.filter(epoch=i)

    decor_list = Decor.objects.order_by('id')
    context = {
        'decor_list': decor_list,
        'epoch_list': epoch_list
    }
    return render(request, "foe/decor.html", context)
    
def road(request):
    epoch_list = Epoch.objects.order_by('id') 
    for i in epoch_list:
        i.road = Road.objects.filter(epoch=i)

    road_list = Road.objects.order_by('id')
    context = {
        'road_list': road_list,
        'epoch_list': epoch_list
    }
    return render(request, "foe/road.html", context) 
    
def production(request):
    epoch_list = Epoch.objects.order_by('id')
    for i in epoch_list:
        i.production = Plant.objects.filter(epoch=i)
        for k in i.production:
            k.production = Production.objects.filter(plant=k)
            for n in k.production:
                n.production_delay_m = n.production_delay / 60
                n.time = 'м'
                if n.production_delay_m >= 60:
                    n.production_delay_h = n.production_delay_m / 60
                    n.time = 'ч'

    production_list = Plant.objects.order_by('id')
    context = {
        'production_list': production_list,
        'epoch_list': epoch_list
    }
    return render(request, "foe/production.html", context)
    
def manufactory(request):
    epoch_list = Epoch.objects.order_by('id')
    for i in epoch_list:
        i.manufactory = Manufactory.objects.filter(epoch=i)
        for k in i.manufactory:
            k.production = Production.objects.filter(manufactory=k)
            k.time_group = ' / '.join(str(n.production_delay / 3600)+'ч' for n in k.production)
            k.res_group = ' / '.join(str(n.resource_cost) for n in k.production)
            k.mon_group = ' / '.join(str(n.money_cost) for n in k.production)
            k.q_group = ' / '.join(str(n.quantity) for n in k.production)
            k.q_group_wo = ' / '.join(str(n.quantity  / 5) for n in k.production)
            
            for n in k.production:
                n.production_delay_h = n.production_delay / 3600
                n.time = 'ч'

    manufactory_list = Manufactory.objects.order_by('id')
    context = {
        'manufactory_list': manufactory_list,
        'epoch_list': epoch_list
    }
    return render(request, "foe/manufactory.html", context)
    
def legend(request):
    epoch_list = Epoch.objects.order_by('id')
    for i in epoch_list:
        i.legend = Legend.objects.filter(epoch=i)
        for k in i.legend:
            k.goodcost = GoodsCost.objects.filter(legend=k)
            k.legend_level = LegendLevel.objects.filter(legend=k)[:1]
            for m in k.legend_level:
                m.legend_bonus = LegendBonusValue.objects.filter(legend_level=m)
            
    legend_list = Legend.objects.order_by('id')
    context = {
        'legend_list': legend_list,
        'epoch_list': epoch_list
    }
    return render(request, "foe/legend.html", context)
    
def legend_detail(request, legend_id):
    legend = Legend.objects.get(pk=legend_id)
    legend.goodcost = GoodsCost.objects.filter(legend=legend)
    legend.legend_level = LegendLevel.objects.filter(legend=legend)
    legend.legend_bonus = LegendLevel.objects.filter(legend=legend)[:1]
    
    for j in legend.legend_bonus:
        j.bonus = LegendBonusValue.objects.filter(legend_level=j)

    for i in legend.legend_level:
        i.legend_bonus = LegendBonusValue.objects.filter(legend_level=i)
        i.prize_first = LegendPrizeValue.objects.filter(legend_level=i).filter(prize_place=1)
        i.prize_second = LegendPrizeValue.objects.filter(legend_level=i).filter(prize_place=2)
        i.prize_third = LegendPrizeValue.objects.filter(legend_level=i).filter(prize_place=3)
        i.prize_forth = LegendPrizeValue.objects.filter(legend_level=i).filter(prize_place=4)
        i.prize_fifth = LegendPrizeValue.objects.filter(legend_level=i).filter(prize_place=5)

    context = {
        'legend': legend,
    }
    return render(request, "foe/legend_detail.html", context)