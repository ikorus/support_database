from django.conf.urls import patterns, url

from foe import views

urlpatterns = patterns('',
  url(r'^$', views.wiki, name='wiki'),
  url(r'^epoch/$', views.epoch, name='epoch'),
  url(r'^epoch/(?P<epoch_id>\d+)/$', views.epoch_detail, name='epoch_detail'),
  url(r'^technology/', views.technology, name='technology'),
  url(r'^houses/', views.houses, name='houses'),
  url(r'^society/', views.society, name='society'),
  url(r'^decor/', views.decor, name='decor'),
  url(r'^road/', views.road, name='road'),
  url(r'^production/', views.production, name='production'),
  url(r'^manufactory/', views.manufactory, name='manufactory'),
  url(r'^legend/$', views.legend, name='legend'),
  url(r'^legend/(?P<legend_id>\d+)/$', views.legend_detail, name='legend_detail'),
)
